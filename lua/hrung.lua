-- lua/hrung.lua

-- Imports plugin Lua modules
local mappings = require("hrung.mappings")
local buffer = require("hrung.buffer")
local handlers = require("hrung.handlers")
local debug = require("hrung.debug")

-- Creates an object for the module
local M = {}

-- Routes calls to functions in other modules

-- mappings module
M.set_mappings = mappings.set_mappings

if debug.on() then -- DEBUG mode on
  -- handlers module
  M.connect_handlers = function()
    handlers.connect_handler('echo')
  end

  -- Main entrypoint function when type CTRL+e
  M.hrung = function()
    local block = buffer.get_block()
    handlers.dispatch('echo', block)
    debug.debug(block)
  end
else -- DEBUG mode off
  -- handlers module
  M.connect_handlers = handlers.connect_enabled_handlers

  -- Main entrypoint function when type CTRL+e
  M.hrung = function()
    -- get the text {block} under the cursor
    local block = buffer.get_block()

    -- identify the proper handler based on {sheband} and/or the code {block}
    local handler = buffer.get_handler(block)

    handlers.dispatch(handler, block)
  end
end

-- Return module's object
return M
