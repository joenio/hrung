-- lua/hrung/handlers.lua

local util = require("hrung.util")
local socket = require("hrung.socket")

-- Creates an object for the module.
local M = {}

-- Internal model for handlers internal state and connection
-- It must be defined after filters, *_handler and *_connect functions
local handlers = {
  echo = {
    name = 'echo',
    enabled = nil,
    connected = false,
    socket = nil,
    port = 42000,
    filters = {util.expand_env_vars},
    connect = socket.tcp_connect,
    handler = socket.write,
  },
  supercollider = {
    name = 'supercollider',
    enabled = nil,
    connected = false,
    socket = nil,
    port = 42001,
    filters = {util.expand_env_vars, util.flat},
    connect = socket.tcp_connect,
    handler = socket.write,
  },
  tidalcycles = {
    name = 'tidalcycles',
    enabled = nil,
    connected = false,
    socket = nil,
    port = 42002,
    filters = {util.expand_env_vars, util.flat},
    connect = socket.tcp_connect,
    handler = socket.write,
  },
  lebiniou = {
    name = 'lebiniou-repl',
    enabled = nil,
    connected = false,
    socket = nil,
    port = 42003,
    filters = {util.expand_env_vars, util.flat},
    connect = socket.tcp_connect,
    handler = socket.write,
  },
  obsstudio = {
    handler = socket.todo,
  },
  sardine = {
    name = 'sardine',
    enabled = nil,
    connected = false,
    socket = nil,
    port = 42004,
    filters = {util.expand_env_vars},
    connect = socket.tcp_connect,
    handler = socket.write,
  },
  naturallanguage = {
    handler = socket.todo
  },
}

-- return an array of strings with the enabled handlers names
-- save the status enabled = true for each discovered handler
local function discover_handlers_enabled()
  local handlers_names = nil
  --return {'echo', 'supercollider'}
  local hrung_handlers = os.getenv("HRUNG_SERVICES")
  if hrung_handlers == nil or hrung_handlers == '' then
    handlers_names = {}
  else
    handlers_names = vim.split(hrung_handlers, "%s")
  end
  for i = 1, #(handlers_names) do
    local handler = handlers_names[i]
    handlers[handler].enabled = true
  end
  return handlers_names
end

function M.connect_handler(handler_name)
  local handler = handlers[handler_name]
  if handler then
    if not handler.connected then
      handler.connect(handler)
    end
  end
end

-- module's public (exported) functions

function M.connect_enabled_handlers()
  local handlers_names = discover_handlers_enabled()
  for i = 1, #(handlers_names) do
    M.connect_handler(handlers_names[i])
  end
end

function M.dispatch(handler, block)
  if handlers[handler] then
    -- apply filters to input block
    if handlers[handler].filters then
      for k, filter in pairs(handlers[handler].filters) do
        block = filter(block)
      end
    end
    -- send the code {block} to the proper {handler}
    handlers[handler].handler(handlers[handler], block)
  else
    error("handler '" .. handler .. "' unknown")
  end
end

-- Return module's object
return M
