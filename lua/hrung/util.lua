-- lua/hrung/util.lua

-- Creates an object for the module.
local M = {}

-- translate any ${VAR} to the value defined in ${VAR} on the environment
function M.expand_env_vars(block)
  -- Lua vim API provides: os.getenv("PWD") AND vim.env.PWD
  local block_env_expanded = string.gsub(block, "${([^%c%p%s]+)}", function (n)
    if os.getenv(n) then
      return os.getenv(n)
    else
      return n
    end
  end)
  return block_env_expanded
end

-- return flat string without newline charachters
function M.flat(block)
  return string.gsub(block, "%c", "")
end

-- Return module's object
return M
