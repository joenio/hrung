-- lua/hrung/buffer.lua

local buf = vim.api.nvim_get_current_buf()
local win = vim.api.nvim_get_current_win()

-- Creates an object for the module.
local M = {}

-- return line number under the cursor
local function get_line_number()
  return vim.api.nvim_win_get_cursor(win)[1]
end

-- return line {n}
local function get_line(n)
  return table.concat(vim.api.nvim_buf_get_lines(buf, n - 1, n, true))
end

-- return a range of lines from line {n} to {m}
local function get_lines(n, m)
  return table.concat(vim.api.nvim_buf_get_lines(buf, n - 1, m, true), "\n")
end

-- get {block} under the cursor
function M.get_block()
  local line_number = nil
  local line_begin = nil
  local line_end = nil
  local lines_count = vim.api.nvim_buf_line_count(buf)
  line_number = get_line_number()
  while line_begin == nil do
    local line = get_line(line_number)
    if line_number == 0 or line_number == nil then
      line_begin = 1
    elseif line == nil or line == '' then
      line_begin = line_number
    else
      line_number = line_number - 1
    end
  end
  line_number = get_line_number()
  while line_end == nil do
    local line = get_line(line_number)
    if line_number >= lines_count then
      line_end = lines_count
    elseif line == nil or line == '' then
      line_end = line_number
    else
      line_number = line_number + 1
    end
  end
  return get_lines(line_begin, line_end)
end

-- get {handler} from the nearest shebang line (#!)
local function get_shebang_handler()
  local line_number = get_line_number()
  local shebang = string.match(get_line(line_number), "^#!(%w%w+)$")
  while shebang == nil do
    line_number = line_number - 1
    if line_number == 0 or line_number == nil then
      return nil
    else
      shebang = string.match(get_line(line_number), "^#!(%w%w+)$")
    end
  end
  return shebang
end

-- apply some regexp patterns to identify the programming language of {block}
-- if the {block} doesn't match with none of the patterns then returns nil
local function get_block_handler(block)
  if string.match(block, "%s*d%d+%s+%$%s+") then
    return "tidalcycles"
  elseif string.match(block, "setcps") then
    return "tidalcycles"
  elseif string.match(block, "%s*SuperDirt%.%w+%s*") then
    return "supercollider"
  elseif string.match(block, "SynthDef") then
    return "supercollider"
  elseif string.match(block, "SinOsc%.ar") then
    return "supercollider"
  elseif string.match(block, "Line%.kr") then
    return "supercollider"
  elseif string.match(block, "s%.record") then
    return "supercollider"
  elseif string.match(block, "s%.mute") then
    return "supercollider"
  elseif string.match(block, "s%.unmute") then
    return "supercollider"
  elseif string.match(block, "s%.boot") then
    return "supercollider"
  elseif string.match(block, "s%.stopRecording") then
    return "supercollider"
  elseif string.match(block, "Pan2%.ar") then
    return "supercollider"
  elseif string.match(block, "LFDNoise3%.kr") then
    return "supercollider"
  else
    return nil
  end
end

-- high level function to detect and take decision about the
-- proper handler to evaluate the current block, it uses shebang but also
-- parses the text block to identify the language in wich the code is written
-- PS: if the no handler is identified then it is supposed
-- to be written in human natural language,
-- ORDER:
-- * block handler has priority over shebang
-- * sheband line has less priority then block

function M.get_handler(block)
  -- get {handler} from code block parsing the text inside the {block}
  local handler = get_block_handler(block)

  -- if {handler} not found on {block} then search the {shebang} line
  if handler == nil or handler == '' then
    -- get {handler} from shebang line
    handler = get_shebang_handler()
  end

  -- if {handler} not found neither on shebang or in the code {block} then return "naturallanguage"
  if handler == nil or handler == '' then
    return "naturallanguage"
  else
    return handler
  end
end

-- Return module's object
return M
