-- lua/hrung/debug.lua

local buffer = require("hrung.buffer")
local handlers = require("hrung.handlers")
local util = require("hrung.util")

local M = {}

function M.debug(block, handler)
  local block = buffer.get_block()
  print("----DEBUG-start------")

  -- get line under cursor and echo
  print(" current-line: " .. vim.api.nvim_get_current_line())

  -- get cursor line number
  print(" line-number: " .. vim.api.nvim_win_get_cursor(win)[1])

  -- single specific line
  print(" line 3: " .. table.concat(vim.api.nvim_buf_get_lines(buf, 3 - 1, 3, true)))

  -- test block inline
  print(" block inline: " .. util.flat(block))

  -- expand environment variables
  print(" block expanded: " .. util.flat(util.expand_env_vars(block)))

  -- detected handler
  print(" handler: " .. buffer.get_handler(block))

  -- print env vars
  print(" $HRUNG_SERVICES: " .. (os.getenv("HRUNG_SERVICES") or ""))
  print(" hrung services: " .. (vim.inspect(vim.split(os.getenv("HRUNG_SERVICES"), "%s")) or ""))
  print("----DEBUG-end------")
end

-- Debug mode defined by env $DEBUG = true
function M.on()
  return os.getenv("DEBUG") == "true"
end

return M
