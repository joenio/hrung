-- lua/hrung/mappings.lua

-- Creates an object for the module
local M = {}

function M.set_mappings()
  local opts = { noremap = true, silent = true }
  print(buf)
  vim.api.nvim_buf_set_keymap(buf, 'n', '<c-e>', ":lua require'hrung'.hrung()<CR>", opts)
  vim.api.nvim_buf_set_keymap(buf, 'i', '<c-e>', "<Esc>:lua require'hrung'.hrung()<CR>i<Right>", opts)
end

-- Return module's object
return M
