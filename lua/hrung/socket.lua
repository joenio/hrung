-- lua/hrung/socket.lua

-- Creates an object for the module.
local M = {}

function M.todo(...)
  print("[TODO] feature not implemented yet!")
end

function M.write(handler, block)
  print("call " .. (handler.name or 'unamed') .. " handler '" .. block .. "'\n")
  vim.loop.write(handler.socket, block)
  vim.loop.write(handler.socket, "\n")
end

function M.tcp_connect(handler)
  handler.socket = vim.loop.new_tcp()
  vim.loop.tcp_connect(handler.socket, "127.0.0.1", handler.port, function (err)
    assert(not err, "socket connection failed 127.0.0.1:" .. handler.port .. " for handler '" .. handler.name .. (err and "' - " .. err or ""))
    handler.connected = true
    vim.loop.read_start(handler.socket, function (err, chunk)
      print("received at client", err, chunk)
      assert(not err, err)
      if chunk then
        print("non-empty chunk")
      else
        print("empty chunk")
      end
    end)
  end)
end

-- Return module's object
return M
