**DEPRECATED** project renamed to dublang, see https://codeberg.org/joenio/dublang-dev

# Hrung

Hrung (or 'The Great Collapsing Hrung Disaster' for long):

* Is a text based user interface
* Does source code live evaluation
* Supports multiple evaluation handlers in a single source code
* Could be defined as 'universal live coding user interface', but no one knows

Hrung is composed by:

* Wrappers around REPL tools, mainly programming languages, but not only
* Some systemd services unit definition to manage wrapers
* A Vim plugin to live dispatch code blocks to interpreters
* Some wrappers to local or remote tools controlled by APIs

## Systemd services

* SuperCollider
* JACK audio server daemon (superseded by PipeWire)
* Tidal Cycles
* Le Biniou
* OBS Studio (TODO)
* Natural Language

## TCP servers (wrappers)

| Service         | TCP port  |
| --------------- | --------- |
| echo (for test) | 42000     |
| supercollider   | 42001     |
| tidalcycles     | 42002     |
| lebiniou-repl   | 42003     |
| sardine-repl    | 42004     |

## Vim plugin

Neovim plugin that evalue the current inner paragraph under the cursor and
dispatch the inner paragraph text to one of the available handlers, handler
will usually execute the code on the paragraph.

The main use case of this plugin is to be used on live coding programming
languages, like TidalCycles, SuperCollider, and others.

1. Paragraph under the cursor is sent to ???
2. The ??? parse the content by static analysis to identify the language
3. The dispatcher reads the Route table and select the proper handler to the identified language
4. The handler executes the code and ...

Good reference about how to run OS proccess from Neovim:
* https://teukka.tech/posts/2020-01-07-vimloop/

## Installation

Enable Hrung Nvim plugin by adding the line below to your
`~/.config/nvim/init.vim`:

```vim
call plug#begin()
Plug 'https://codeberg.org/joenio/hrung.git'
call plug#end()
```

Depends on [vim-plug](https://github.com/junegunn/vim-plug).

### NeoVim installation

```sh
sudo apt install neovim lua-nvim
```

### Usage

```
:TidalcyclesConnect
```

## Development

```sh
sudo apt install lua5.2 lua-lpty lua-luv-dev
```

We assume you have the Hrung repository cloned in your local machine at
`$HOME/src/hrung`.

### Local Usage

Enable Hrung Nvim plugin from your local repository clone by adding the line
below to your `~/.config/nvim/init.vim`:

```vim
call plug#begin()
Plug '~/src/hrung'
call plug#end()
```

## Hrung on command-line

**ALERT** This is a experimental interface just for prototyping and might be
rewritted in some programming language soon.

Before start using the Hrung Neovim plugin it is needed to start the services.

```sh
make start-all
```

Others useful commands:
- `make top` - Open a monitor with the status of each service
- `make logs` - Follow services log messages
- `make stop-all` - Stop all services

### Managing services

To enable 1 single service run.

```sh
SERVICE=lebiniou-repl make start
```

It is possible to configure a list of services in the environment variable
`$HRUNG_SERVICES`.

```sh
export HRUNG_SERVICES='lebiniou-repl supercollider'
make start-all
```

#### File `.env` (alpha)

It is recommended to create a file `.env` in the root of hrung source code to
define what services are enabled, it will define systemd services at Makefile
level, also will be considered by vim plugin to decide if connect or not to
each available service/handler.

```sh
HRUNG_SERVICES=lebiniou-repl supercollider
```

Load file into shell session and run nvim.

```sh
source .env
export HRUNG_SERVICES
nvim main.hrung
```

Hrung will try to connect only on the services defined by `$HRUNG_SERVICES`.

## Using OBS

OBS is useful not only for streaming or recording but also to play in offline performances,
with OBS is possible to combine windows in layers using fx-effects to merge them.

For eg if I have 2 windows:

- Le Biniou with thevisuals
- Terminal + nvim + hrung

Then it is possible to add both at OBS scene, configure Le Biniou on top of
Terminal and apply a merge effect on Le Biniou.

## References

* [The Great Collapsing Hrung Disaster](https://hitchhikers.fandom.com/wiki/Great_Collapsing_Hrung_Disaster)
* [SuperCollider](https://supercollider.github.io)
* [JACK Audio Connection Kit](https://jackaudio.org)
* [Le Biniou](https://biniou.net)
* [Tidal Cycles](https://tidalcycles.org)

## Author

Joenio Marques da Costa

## License

This project is licensed under the GNU General Public License v3.0 or later - see the COPYING file for details.
