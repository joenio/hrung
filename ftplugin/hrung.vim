" ftplugin/hrung.vim

" Title:        Hrung Plugin
" Description:  The Great Collapsing Hrung Disaster
" Last Change:  $LAST_CHANGE
" Maintainer:   Joenio Marques da Costa <joenio@joenio.me>

if exists('g:loaded_hrung') | finish | endif " prevent loading plugin twice

let s:save_cpo = &cpo " save user coptions
set cpo&vim " reset them to defaults

" command to the plugin
lua require'hrung'.set_mappings()
lua require'hrung'.connect_handlers()

" Exposes the plugin's functions for use as commands in Neovim.
command! -nargs=0 TidacyclesConnect lua require'hrung'.tidalcycles_connect()
" TODO add all handlers and also a global connect to connect to all in one shot

" haskell.vim syntax options
" see https://neovim.io/doc/user/syntax.html#haskell.vim
let hs_allow_hash_operator = 1

let &cpo = s:save_cpo " and restore after
unlet s:save_cpo

let g:loaded_hrung = 1
