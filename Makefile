HRUNG_SERVICES ?= $(subst services/,,$(patsubst %.service,%,$(wildcard services/*.service)))

SERVICES := ${HRUNG_SERVICES}

# Makefile functions
systemctl_status = (systemctl --user --no-pager --lines 1 status $1.service)

help:
	@echo "Hrung (or 'The Great Collapsing Hrung Disaster')"
	@echo
	@echo "USAGE:"
	@echo
	@echo "  make <command> [SERVICE=<service>]"
	@echo
	@echo "COMMANDS:"
	@echo
	@echo "  list             list ststemd services available (option SERVICE mandatory)"
	@echo "  install          install systemd service at userspace (option SERVICE mandatory)"
	@echo "  enable           enable systemd service at userspace (option SERVICE mandatory)"
	@echo
	@echo "SERVICES:"
	@echo
	@echo "- jackd            ???"
	@echo "- supercollider    ???"
	@echo "- tidalcycles      ???"
	@echo
	@echo "EXAMPLES:"
	@echo
	@echo "  Start jackd daemon:"
	@echo "  make start SERVICE=jackd"

daemon-reload:
	systemctl --user daemon-reload

# all

install-all:
	@$(foreach service,$(SERVICES), make install SERVICE=$(service);)

remove-all:
	@$(foreach service,$(SERVICES), make remove SERVICE=$(service);)

enable-all:
	@$(foreach service,$(SERVICES), make enable SERVICE=$(service);)

disable-all:
	@$(foreach service,$(SERVICES), make disable SERVICE=$(service);)

start-all:
	@$(foreach service,$(SERVICES), make start SERVICE=$(service);)

stop-all:
	@$(foreach service,$(SERVICES), make stop SERVICE=$(service);)

status-all:
	@$(foreach service,$(SERVICES), $(call systemctl_status, $(service));)

reload-all:
	@$(foreach service,$(SERVICES), make reload SERVICE=$(service);)

restart-all:
	@$(foreach service,$(SERVICES), make restart SERVICE=$(service);)

top: monitor
monitor:
	./bin/monitor

reinstall-all: remove-all install-all reload-all

# list

list:
	@echo "Hrung (or 'The Great Collapsing Hrung Disaster')"
	@echo
	@echo "SERVICES available:"
	@echo
	@$(foreach service,$(SERVICES), echo - $(service);)

# validate

validate:
	@test -n "${SERVICE}" || echo "service not defined"
	@test -e services/${SERVICE}.service || echo "service not found"
	@test -n "${SERVICE}" && test -e services/${SERVICE}.service && echo "service is valid"

# service

install: validate
	@mkdir -p ${HOME}/.config/systemd/user
	envsubst < services/${SERVICE}.service > ${HOME}/.config/systemd/user/${SERVICE}.service

remove: validate disable
	rm -f ${HOME}/.config/systemd/user/${SERVICE}.service || true

enable: validate
	systemctl --user enable ${SERVICE}.service

disable: validate
	systemctl --user disable ${SERVICE}.service || true

start: validate
	systemctl --user start ${SERVICE}.service

stop: validate
	systemctl --user stop ${SERVICE}.service

status: validate
	@$(call systemctl_status, ${SERVICE})
	@echo

reload: validate daemon-reload
	systemctl --user force-reload ${SERVICE}.service

reinstall: remove install reload

restart: stop start

# logs

logs:
	journalctl --user $(foreach service,$(SERVICES),-u $(service).service) -f

