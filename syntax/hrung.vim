" Vim syntax file
" Language: hrung
" Maintainer: Joenio M. Costa <joenio@joenio.me>

" TODO https://vim.fandom.com/wiki/Creating_your_own_syntax_files
" based on https://github.com/tidalcycles/vim-tidal/blob/master/syntax/tidal.vim

if exists("b:current_syntax")
  finish
endif

"syn sync fromstart

syn include @TidalCycles syntax/haskell.vim
unlet b:current_syntax
syn region tidalcycles start="^#!tidalcycles$" end="^#!end" contains=@TidalCycles keepend

syn include @SuperCollider syntax/supercollider.vim
unlet b:current_syntax
syn region supercollider start="^#!supercollider$" end="^#!end" contains=@SuperCollider keepend

syn match hrungShebang "^#!.\+$" containedin=ALL contained
highlight def link hrungShebang Macro

syn match tidalPattern "^d[0-9]\|^p \".\+\"\|^p '.\+'\|^\s\+d[0-9]" containedin=@TidalCycles contained
highlight def link tidalPattern Function

" color and style definitions

" shebang blue bg, white fg, bold
highlight Macro cterm=bold,reverse gui=bold,reverse ctermfg=Blue guifg=Blue
" line number blue bg, light blue fg
highlight LineNr ctermbg=Blue guibg=Blue ctermfg=LightBlue guifg=LightBlue
" tidal patterns d1, d2, p 'beat', p 'name', etc
highlight Function cterm=bold,reverse gui=bold,reverse ctermfg=DarkCyan guifg=DarkCyan

let b:current_syntax = "hrung"
